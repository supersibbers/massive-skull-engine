INCLUDE DailyRoutine.ink
INCLUDE ledgermain_introduction.ink
INCLUDE young_people_introduction.ink
INCLUDE generic_interaction.ink
INCLUDE traits.ink
INCLUDE romantic_walk_with_ledgermain.ink
INCLUDE tunnel_experiment.ink

EXTERNAL get_char_reference()
EXTERNAL get_storylet_options(x, y)

VAR money = 0
VAR name = "the outsider"
VAR number_of_days = 0
VAR story_started = 0
VAR return_to = ""

-> intro

=== intro ===
this is the start of the game

you meet Dagmar
~dagmar_in_story = true

~ story_started = 1

-> start_of_day


//real functions

== function go_to_moment(type, return_address)
    ~ return_to = return_address
    ~ get_storylet_options(0, type)

//placeholders for external functions

== function get_storylet_options(x, y) ==
~ return 1 // placeholder result

== function get_char_reference() ==
~ return 1 // placeholder result


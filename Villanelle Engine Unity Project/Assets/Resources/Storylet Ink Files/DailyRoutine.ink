VAR shop_visited = false
VAR bought_cake = false
VAR visited_pub = false

=== start_of_day ===

It's the morning of a new day. You have {money} coins.

{ money == 0:
	You are broke.
}

+ [continue]
    -> root_options


=== root_options ===

{get_storylet_options(4, "root")} 

->DONE

=== work ===

You go to work.

At the end of a hard day's graft, you are given a shiny coin.

~ money = money + 1

-> bed_time -> start_of_day



=== shop ===

You go to the shop and get a cake. Someone invites you to the pub.

~ shop_visited = true
~ bought_cake = true

-> bed_time -> start_of_day


=== pub ===

You go to the pub.

Everyone loves that you have a cake.

~ money = money - 1

-> bed_time -> start_of_day


=== desolation_row ===

Nobody here has much to say to you.

-> bed_time -> start_of_day

=== bed_time ===

Eventually you go home and go to bed.

+ [continue] 
    ->->
=== tunnel_experiment ===

something happens, then we divert into a tunnel

~go_to_moment("moment", -> end_of_tunnel_experiment)

->DONE

=== end_of_tunnel_experiment ===

and then we're back from the tunnel

-> bed_time -> start_of_day


=== test_moment ===

EMPTY_LINE

words happen

we see a lovely detail here

-> return_to

=== another_moment ===

EMPTY_LINE

we see a different detail

and it's lovely

-> return_to
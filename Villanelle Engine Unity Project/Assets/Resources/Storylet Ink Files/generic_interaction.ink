
=== generic_interaction ===

~ setup_char()

{char_name} walks up to you.

{char_funny: {She} makes a joke.| {She} doesn't make a joke.}

+Be nice
    ~ char_affinity += 1
    {She} seems happy with you.
+Don't be nice
    ~ char_affinity -= 1
    {She} isn't enjoying herself. {Her_p} affinity is now {char_affinity}.

- Then the scene ends.

-> bed_time -> start_of_day

=== funny_generic_bit ===
~ setup_char()

{char_name} walks up to you and does some fun goofs.

-> bed_time -> start_of_day

=== bit_with_friend ===
~ setup_char()

{char_name} has good affinity with you.

-> bed_time -> start_of_day


=== funny_love_bit ===
~ setup_char()

{char_name} has affinity {char_affinity} with you and has found a funny way to make their boundless affection known.

-> bed_time -> start_of_day


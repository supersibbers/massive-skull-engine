

-> young_people_introduction

=== young_people_introduction ===

You approach the gate of the labyrinth. It's perfunctory, rectangular, unornamented. Inside, the passageway runs off to the left and the right - the same blue-grey stone as the exterior.

The grass around the entrance is bushy and overgrown. A thin path where the grass has been trodden thin leads from the road seems to lead to the gateway, but veers of at the last moment and ends beside it, where a figure now leans against the wall, their face hidden by a hood, a spear in their hand.

Nearby, in the bushes, you hear the sound of quiet voices talking insistently.

* I approach the entrance. -> entrance
* I approach the figure. -> guard
* I approach the voices. -> voices

- (entrance)

-> DONE


- (guard)

-> DONE

- (voices)

-> DONE
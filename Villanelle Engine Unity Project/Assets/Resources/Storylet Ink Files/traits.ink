VAR char_reference = ""

VAR char_affinity = 0
VAR char_name = ""
VAR char_funny = false
VAR char_gender = "male"
VAR char_in_story = false

VAR she = "she"
VAR her = "her"
VAR her_p = "her"
VAR hers = "hers"
VAR She = "She"
VAR Her = "Her"
VAR Her_p = "Her"
VAR Hers = "Hers"

VAR ledgermain_affinity = 0
VAR ledgermain_name = "Ledgermain"
VAR ledgermain_funny = false
VAR ledgermain_in_story = false

VAR dagmar_affinity = 0
VAR dagmar_name = "Dagmar"
VAR dagmar_funny = true
VAR dagmar_in_story = false

=== function setup_char() ===
~ get_char_reference()

{ char_reference:
- "ledgermain":
    ~ char_affinity = ledgermain_affinity
    ~ char_name = ledgermain_name
    ~ char_funny = ledgermain_funny
    ~ char_gender = "male"
- "dagmar": 
    ~ char_affinity = dagmar_affinity
    ~ char_name = dagmar_name
    ~ char_funny = dagmar_funny
    ~ char_gender = "female"
}

~ designate_pronouns(char_gender)

=== function designate_pronouns(gender) ===

{ gender:
- "male":
    ~ she = "he"
    ~ her = "him"
    ~ her_p = "his"
    ~ hers = "his"
    ~ She = "He"
    ~ Her = "Him"
    ~ Her_p = "His"
    ~ Hers = "His"
- "female":
    ~ she = "she"
    ~ her = "her"
    ~ her_p = "her"
    ~ hers = "hers"
    ~ She = "She"
    ~ Her = "Her"
    ~ Her_p = "Her"
    ~ Hers = "Hers"
}





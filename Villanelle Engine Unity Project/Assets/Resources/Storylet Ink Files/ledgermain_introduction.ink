// on implementation - ledgermain_mentions_young_people offers alternate route into first young people scene, and that scene can provide this with a different ending which will need to be written after I know who they are


// EDITORIAL: This scene seems to want to be about establishing the attitudes of the townspeople to the tower, the labyrinth, the taboo around spending time there, etc. Play more into that, particularly when he introduces himself, and when he talks about the tower. Easier once I understand the tabboo a little better.

-> ledgermain_introduction

=== ledgermain_introduction ===
As you make your way out of the village, you see a broad, hunched-over man wrapped up in a thick grey cloak. Two stony eyes and a mossy beard protrude, and you can see where, under the cloak, he is gripping the material tightly in his fists.

As you pass close by him, he hails you.

"You're {name}, aren't you? I've heard of you."
* "I am."[] He grunts and nods his head.
* (affront) "What's it to you?"[] 
    His eyebrows wrinkle and his lips tighten for a moment.
    "It's neither here nor there to me," he says.
    "And who are you, then?"
* "Apparently everyone's head of me."[] 
    "Yes, it's true. They have." 
    "It puts me at a disadvantage," you reply. "I feel like everything I do or say becomes a topic of conversation, and I rarely know who it is I'm actually talking to."

- "Well, I'm Ledgermain." he ventures{affront:, "Frankly, it matters not a jot to me who you are. I only asked out of courtesy."|.}

A moment passes. He mutters something inaudible. He seems to be launching into a lecture, but he talks down, into his beard, and over the sound of the wind you can't make out what he's saying.

~ temp rude = false

* "Pardon me?" 
    -> talk
* "Speak up, old man."
    ~ rude = true
   -> talk
* I stand silently and wait. -> no_talk
* I stand silently and roll my eyes. 
    ~ rude = true
    ->no_talk

- (talk) 
"I said, that's my tower over there." {rude: This is halfway a yell. "No manners where you come from, are there?" |He smiles kindly, though his thicket of beard. "I'm sorry, I don't see many people and in private I have a tendency to muttering."}

    -> tower_point

- (no_talk)
He continues to mutter, his eyes moving from you to the village in the distance at your back. He pauses when a bird cries out and he turns his head to listen, and then with renewed gusto resumes his quiet speaking, gesturing with his hands. 

    -> tower_point

- (tower_point)

He points up the road, in the direction you were walking. On the horizon is a small stone tower with a flat roof, half wrapped in ivy of a similar grey-green colour to the man's cloak. {rude: He frowns at you as he points, his eyebrow raised impreiously.}

~ temp apologise = false

//chat about tower where you can ask questions
- (options)
* {talk||apologise}"You live in that tower?"
    "Yes, for many years. It's my only real connection to how things were before, you see."
    "What do you mean, how things were before?"
    "Well, you know, the time of this..." He gestures at the labyrinth, the giant skull. "And before all of this." He gestures at the village, the fishing boats bobbing out to sea.
    "You're older than the village?"
    "No, of course not! I was born in the village."
    "But the villagers avoid the labyrinth and everything assosciated with it. How are you happy to live somewhere from that time?"
    "Well, now. I'm not suggesting I'd gladly enter the labyrinth itself. But my tower seems benign enough. I've lived there for many years with no mishap."
* (thing){rude && !loop} "I seem to have annoyed you."
    "Me? Annoyed by an insignificant piece of thing like you? Hardly."
* {rude && !loop} "I seem to have annoyed you, and I apologise."
    ~ apologise = true
    He nods deeply. 
    "Your apology is accepted."
* "Who built it?"
    "Well, how should I know? It's very ancient, you see. Perhaps him?" He points at the giant skull. "Every night, I go to bed hoping the answer will come to me in a dream. Such things aren't unheard of."
* {loop}"I'm going to go. I want to see a little more of the landscape before I retire for the night." 
    "Yes, yes, right you are. You're not the only soul with ambitions for the rest of the day."
    "Indeed not."
    -> goodbye

-(loop)
{ -> options | }
	He scratches his head.
	"Well, can't stand around talking all day," he declares. "I'm sure we'll cross paths again."
	"I'm sure we will."

- (goodbye)
{rude: "You seem to be an outspoken, direct sort of person, and you seem not to mind what people think of you. | "Oh, before you go. }
<> If you'd like me to owe you a favour, I have something that needs sorting. I don't know if you're looking for work, or..."
    *"Yes, I am looking for work, as it happens."
        VAR ledgermain_mentions_young_people = true
        "Ah, good. Well, as it stands, there are some young people from the village, you see, no standards, and they've been spending time by the gate to the labyrinth, often at night. I've seen them. I wonder if a strapping outsider like yourself could put it to them to take their revels somewhere more proper?"
        {thing: "A strapping outsider? I thought I was a mere piece of thing. But sure, I'll see what I can do."|"Well, I'll certainly have a go."}
        "Good, good." 
    *"No, I'm not interested."
        "Suit yourself."
    // *{spoken_to_youths}"Dagmar and her lot? They're harmless."



- He nods once more, and then pulling his cloak ever tighter around his round shoulders, he sets off towards the village. 

~ ledgermain_in_story = true

{rude: 
    ~ ledgermain_affinity -= 1
}

-> bed_time -> start_of_day





=== ledgermain_story ===

"I said all this, and you know what she said?"

"What?"

"She said 'you've got crumbs in your beard'." Then she turned and walked away.

He puts a hand to his face, as if feeling to see if any crumbs yet remain.


->->






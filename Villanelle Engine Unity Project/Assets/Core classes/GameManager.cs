﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Ink.Runtime;
using System;

public class GameManager : MonoBehaviour
{
    UIController uiController;

    // Singleton stuff
    public static GameManager instance { get; private set; }

    // reference to playerstate
    PlayerState playerState;

    //reference to referencecharactermanager
    ReferenceCharacterManager referenceCharacterManager;

    // Ink stuff
    public TextAsset masterInkJSON;
    private Story inkStory;

    // the next batch of content to send to the UI
    private List<DisplayableContent> nextContentList = new List<DisplayableContent>();

    //create single instance
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {

        uiController = GameObject.FindGameObjectWithTag("UIController").GetComponent<UIController>();

        playerState = PlayerState.instance;

        referenceCharacterManager = ReferenceCharacterManager.instance;

        SetupInkStory();

        LoadStorylets loader = new LoadStorylets();
        loader.Load();

        CreateStartOptions();

    }

    public void CreateStartOptions()
    {
        //make a start button that gets some root options
        List<DisplayableContent> contentList = new List<DisplayableContent>();
        DisplayableButton choiceButton = new DisplayableButton("start", () => GoToInkKnotFromKnotName("intro"));
        contentList.Add(choiceButton);
        DisplayableButton choiceButton2 = new DisplayableButton("start sample story", () => GoToInkStoryFromFilename("InkleSampleStory"));
        contentList.Add(choiceButton2);

        SendToClearedUI(contentList);
    }

    public void SetupInkStory()
    {
        //setup inital ink json
        inkStory = new Story(masterInkJSON.text);

        // bind external functions
        inkStory.BindExternalFunction("get_storylet_options", (int number, string type) =>
        {
            GetStoryletOptions(number, type);
        });

        inkStory.BindExternalFunction("get_char_reference", () =>
        {
            inkStory.variablesState["char_reference"] = referenceCharacterManager.GetCharacterRefForReferenceScene();
        });

        // observe ink variables
        inkStory.ObserveVariable("char_affinity", (string varName, object newValue) =>
        {
            UpdateAffinity(referenceCharacterManager.currentCharacter, (int)newValue);
        });

        inkStory.ObserveVariable("char_in_story", (string varName, object newValue) =>
        {
            UpdateInStory(referenceCharacterManager.currentCharacter, (int)newValue);
        });


        playerState.LoadDefaultParameters();

        UpdateInkWithDefaultParameters();

    }

    private void UpdateInkWithDefaultParameters()
    {
        foreach (Parameter param in playerState.inkParameterList)
        {

            if (param.ValueType != Parameter.Type.String)
            {
                inkStory.variablesState[param.Key] = int.Parse(param.Value);
            }
            else
            {
                inkStory.variablesState[param.Key] = param.Value;
            }
        }
    }

    private void GetStoryletOptions (int number, string type)
    {
        //Debug.Log("Ink is calling for some externally calculated root options.");

        //read the game state
        playerState.ReadGlobalParameters(inkStory.variablesState);

        //foreach (Parameter param in playerState.inkParameterList)
        //{
        //    Debug.Log(param.Key + " " + param.Value);
        //}

        //if (playerState.FindParamByKey("money") != null) { Debug.Log("money param exists");}

        //Debug.Log(playerState.inkParameterList.Count);

        //generate some options
        SelectionAlgorithm(number, type);

    }

    private void UpdateAffinity(Character character, int newValue)
    {
        inkStory.variablesState [character.reference +"_affinity"] = newValue;
    }

    private void UpdateInStory(Character character, int newValue)
    {
        inkStory.variablesState[character.reference + "_in_story"] = newValue;
    }



    private List<DisplayableContent> tempContentList;

    public void SelectionAlgorithm(int n, string type)
    {

        //make somewhere to put storylets once we've chosen them
        tempContentList = new List<DisplayableContent>();


        //get all storylets of appropriate type from storylet manager
        List<Storylet> storylets = StoryletManager.GetStoryletsByType(type);

        //filter them by predicate
        List<Storylet> filteredStorylets = new List<Storylet>();

        
        foreach (Storylet storylet in storylets)
        {
            //reset the storylet's list of acceptale characters
            storylet.currentAvailableCharacters = new List<Character>();

            if (!storylet.hasPredicates)
            {
                filteredStorylets.Add(storylet);
            }
            else
            {
                if (CheckAllPredicatesSatisfied(storylet))
                {
                    filteredStorylets.Add(storylet);
                }
            }
        }

        if (filteredStorylets.Count == 0)
        {
            Debug.LogError("Your list of possible storylets in this moment is empty!");
        }

        //shuffle the list
        System.Random rng = new System.Random();

        //shuffling algo from stackoverflow
        int _n = filteredStorylets.Count;
        while (_n > 1)
        {
            _n--;
            int k = rng.Next(_n + 1);
            Storylet value = filteredStorylets[k];
            filteredStorylets[k] = filteredStorylets[_n];
            filteredStorylets[_n] = value;
        }

        if (n == 0) // if n is 0, just go straight to the first option and redirect the story there
        {

            inkStory.ChoosePathString(filteredStorylets[0].knotName);

        }
        else // otherwise, make buttons
        {
            //for each n, create a button and send it to UI
            //SELECTION ALGORITHM GOES HERE
            for (int i = 0; i < (int)Mathf.Min(filteredStorylets.Count, n); i++)
            {
                //each button will need to load the relevant ink file into inkStory and then sendnextstorychunktoUI

                int _i = i;
                filteredStorylets[i].timesOffered += 1;

                string buttonText = filteredStorylets[i].description + " Offered: " + filteredStorylets[i].timesOffered + " Visited: " + filteredStorylets[i].timesVisited;

                DisplayableButton choiceButton = new DisplayableButton(buttonText, () => GoToStorylet(filteredStorylets[_i]));
                tempContentList.Add(choiceButton);


            }

            //add them to the next content list so they get sent to the UI once this stack resolves
            AddToNextContentList(tempContentList);
        }

    }

    private bool CheckAllPredicatesSatisfied(Storylet storylet)
    {
        bool allPredicatesSatisfied = true;

        foreach (Predicate predicate in storylet.predicates)
        {

            //if predicate is met, stay true
            //else (i.e. predicate isn't satisfied) set result to false
            if (!CheckIndividualPredicateSatisfied(storylet, predicate)) {allPredicatesSatisfied = false;}

        }
        return allPredicatesSatisfied;
    }

    private bool CheckIndividualPredicateSatisfied(Storylet storylet, Predicate predicate)
    {
        if (predicate.isCharacterPredicate){
            return CheckCharacterPredicate(storylet, predicate);
        }
        else
        {
            return CheckNonCharacterPredicate(predicate);
        }
    }

    private bool CheckCharacterPredicate(Storylet storylet, Predicate predicate)
    {
        //Debug.Log("checking character predicate " + predicate.Key);

        if (predicate.Key.Substring(0,3) == "sel")
        {
            foreach (Character character in storylet.currentAvailableCharacters)
            {
                string paramNameToCheck = character.reference + predicate.Key.Substring(3);

                //Debug.Log("in selected: looking for a param called " + paramNameToCheck + "to compare it with");

                Parameter parameterToCheck = playerState.FindParamByKey(paramNameToCheck);

                //Debug.Log("in selected: about to test " + parameterToCheck.Key + "parameter against this predicate");

                if (CompareParameterAndPredicate(parameterToCheck, predicate)) { return true; }
                else
                {
                    storylet.currentAvailableCharacters.Remove(character);
                    return false;
                }
            }
            //if here, there are 0 people in the sub-list so just return false
            return false;
        }
        //if the predicate starts with an underscore, it's an "any" and we need to look at all the available characters
        else if (predicate.Key.Substring(0,1) == "_" || predicate.Key == "any")
        {
            storylet.currentAvailableCharacters = new List<Character>();

            //for each character in the story, check their relevant param (using CompareParameterAndPredicate) and add them to currently available characters
            //at the end, if it's not empty, return true

            foreach (Character character in referenceCharacterManager.characterList)
            {
                if (character.inStory)
                {
                    string paramNameToCheck;

                    if (predicate.Key != "any")
                    {
                        paramNameToCheck = character.reference + predicate.Key;
                    }
                    else
                    {
                        paramNameToCheck = character.reference + "_in_story";
                    }

                    //Debug.Log("looking for a param called " + paramNameToCheck + "to compare it with");

                    Parameter parameterToCheck = playerState.FindParamByKey(paramNameToCheck);

                    //Debug.Log("about to test " + parameterToCheck.Key + "parameter against this predicate");

                    if (CompareParameterAndPredicate(parameterToCheck, predicate)) { storylet.currentAvailableCharacters.Add(character); }
                }
            }

            if (storylet.currentAvailableCharacters.Count != 0) { return true; }
            else { return false; }

        }
        //else, it should just be a name so we check if that character is in the story (scene isn't generic)
        else
        {
            string name = predicate.Key;

            Character character = referenceCharacterManager.FindCharacterByName(name);

            if (character.inStory)
            {
                storylet.currentAvailableCharacters.Add(character);
                return true;
            }
            else
            {
                return false;
            }
            
        }
    }

    private bool CheckNonCharacterPredicate(Predicate predicate)
    {
        //Debug.Log("checking normal predicate " + predicate.Key);

        Parameter parameterToTest = playerState.FindParamByKey(predicate.Key);

        return CompareParameterAndPredicate(parameterToTest, predicate);
    }


    private bool CompareParameterAndPredicate(Parameter parameterToTest, Predicate predicate)
    {
        //Debug.Log("comparing " + parameterToTest.Key + " and " + predicate.Key);

        if (parameterToTest.ValueType == Parameter.Type.Int)
        {

            switch (predicate.Operator)
            {
                case Predicate.PredicateOperator.equals:
                    return (int.Parse(parameterToTest.Value) == int.Parse(predicate.Value));
                case Predicate.PredicateOperator.greaterthan:
                    return (int.Parse(parameterToTest.Value) > int.Parse(predicate.Value));
                case Predicate.PredicateOperator.lessthan:
                    return (int.Parse(parameterToTest.Value) < int.Parse(predicate.Value));
                case Predicate.PredicateOperator.greaterthanequals:
                    return (int.Parse(parameterToTest.Value) >= int.Parse(predicate.Value));
                case Predicate.PredicateOperator.lessthanequals:
                    return (int.Parse(parameterToTest.Value) <= int.Parse(predicate.Value));
                default:
                    Debug.LogWarning("Predicate " + predicate.Key + " doesn't seem to have a valid operator. Mysterious.");
                    return false;
            }
        }
        else //i.e. it's a bool or a string - simple comparison
        {
            return (parameterToTest.Value == predicate.Value);
        }
    }


    void AddToNextContentList(List<DisplayableContent> list)
    {
        list.ForEach(item => nextContentList.Add(item));
    }

    public void GoToStorylet(Storylet storylet)
    {

        storylet.timesVisited += 1;

        playerState.AdjustIntParameter(storylet.knotName, 1);

        string knotName = storylet.knotName;

        //if storylet.usesGenericCharacter, record that

        //send the currentAvailableCharacters to the reference character manager so it can assign the right one in a minute
        if (storylet.usesGenericCharacter)
        {
            referenceCharacterManager.charactersAvailableForNextStorylet = storylet.currentAvailableCharacters;
        }

        GoToInkKnotFromKnotName(knotName);
    }

    public void GoToInkKnotFromKnotName(string knotName)
    {
        inkStory.ChoosePathString(knotName);
        SendNextInkStoryChunkToUI();

    }

    public void GoToInkStoryFromFilename(string filename)
    {
        //get textasset with corresponding filename
        TextAsset newInkStory = Resources.Load<TextAsset>("Storylet Ink Files/" + filename);
        inkStory = new Story(newInkStory.text);
        SendNextInkStoryChunkToUI();

    }

    string textOfNextLine;

    string textOfPreviousLine;

    private void ContinueStory()
    {

        textOfNextLine = inkStory.Continue();

        nextContentList.Add(new DisplayableText(textOfNextLine));

        if (inkStory.canContinue)
        {
            ContinueStory();
        }

        textOfPreviousLine = textOfNextLine;
    }

    private void SendNextInkStoryChunkToUI()
    {
    
        nextContentList = new List<DisplayableContent>();

        if (inkStory.canContinue)
        {
            ContinueStory();
        }

        if (inkStory.currentChoices.Count > 0)
        {
            for (int i = 0; i < inkStory.currentChoices.Count; ++i)
            {

                int _i = i;
                DisplayableButton choiceButton = new DisplayableButton(inkStory.currentChoices[i].text, () => MakeChoice(_i));
                nextContentList.Add(choiceButton);

            }
        }

        //foreach (DisplayableContent item in nextContentList)
        //{
        //    Debug.Log(item.text);
        //}

        SendToClearedUI(nextContentList);
    }

    public void SendToClearedUI(List<DisplayableContent> displayableContent)
    {

        //send a list of DisplayableContent to the UI controller

        uiController.content = displayableContent;
        uiController.ContentChanged();

    }

    public void MakeChoice(int i)
    {
        inkStory.ChooseChoiceIndex(i);
        SendNextInkStoryChunkToUI();
    }

}

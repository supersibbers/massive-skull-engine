﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;

// holds narrative parameters

public class PlayerState : MonoBehaviour
{
    public static PlayerState instance { get; private set; }

    public List<Parameter> inkParameterList;

    public TextAsset parameterDefaults;

    private GameManager gameManager;

    private ReferenceCharacterManager referenceCharacterManager;
    
    //create single instance
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        gameManager = GameManager.instance;

        referenceCharacterManager = ReferenceCharacterManager.instance;


    }


    public void ReadGlobalParameters(VariablesState state)
    {
        foreach (string parameter_name in state)
        {
            //Debug.Log("Synching a parameter called "+ parameter_name);
            SynchParameterFromInk(parameter_name, state);
        }

        referenceCharacterManager.UpdateCharactersFromInkState();
    }

    private void SynchParameterFromInk(string parameter_name, VariablesState state)
    {
        //see if we already have a parameter called parameter_name

        bool parameter_already_present = false;

        Parameter parameter = new Parameter();

        foreach (Parameter param in inkParameterList)
        {
            if (parameter_name == param.Key)
            {
                parameter_already_present = true;
                parameter = param;
            }
        }

        if (parameter_already_present)
        {
            // if we do, update its value from the story
            //Debug.Log("Parameter " + parameter_name + " found. Updating to match Ink version.");

            if (state[parameter_name].GetType() == typeof(int))
            {
                parameter.Value = state[parameter_name].ToString();
            }
            else if (state[parameter_name].GetType() == typeof(string))
            {
                parameter.Value = (string) state[parameter_name];
            }

        }
        else
        {
            // if we don't, create it and set its value from the story
            //Debug.Log("Parameter " + parameter_name + " not found. Creating it.");

            if (state[parameter_name].GetType() == typeof(int))
            {
                parameter = new IntParameter(parameter_name, (int)state[parameter_name]);
            }
            else if (state[parameter_name].GetType() == typeof(bool))
            {
                parameter = new BoolParameter(parameter_name, (bool)state[parameter_name]);

            }
            else if (state[parameter_name].GetType() == typeof(string))
            {
                parameter = new StringParameter(parameter_name, (string)state[parameter_name]);

            }
            else
            {
                Debug.LogWarning("Attempting to create parameter of unknown type!");
                parameter = new Parameter();
            }

            inkParameterList.Add(parameter);

        }
    }

    public Parameter FindParamByKey(string key)
    {
        //Debug.Log("Searching for param called " + key);

        bool foundIt = false;

        foreach (Parameter param in inkParameterList)
        {
            if (key == param.Key)
            {
                foundIt = true;
            }
            else
            {
                foundIt = false;
            }

            if (foundIt)
            {
                //Debug.Log("found it");
                return param;
            }

        }
        Debug.LogWarning("You're looking for a parameter called " + key + " that hasn't been initialised. It may be missing from the spreadsheet or never created.");
        return null;
    }


    public void LoadDefaultParameters()
    {
        List<Parameter> parameters = new List<Parameter>();

        string[] data = parameterDefaults.text.Split(new char[] { '\n' });

        for (int i = 1; i < data.Length - 1; i++)
        {

            string[] row = data[i].Split(new char[] { ',' });

            if (row[1] == "bool")
            {

                bool value;

                if (row[2] == "TRUE")
                {
                    value = true;
                }
                else
                {
                    value = false;
                }

                string name = row[0];

                Parameter p = new BoolParameter(name, value) as Parameter;

                parameters.Add(p);

            }
            else if (row[1] == "int")
            {
                int value = int.Parse(row[2]);
                string name = row[0];

                Parameter p = new IntParameter(name, value) as Parameter;

                parameters.Add(p);
            }
            else if (row[1] == "string")
            {
                string name = row[0];
                string value = row[2];

                Parameter p = new StringParameter(name, value) as Parameter;

                parameters.Add(p);
            }
        }


        inkParameterList = parameters;

    }


    public void CreateIntParameter(string key, int value)
    {
        Parameter p = new IntParameter(key, value) as Parameter;

        inkParameterList.Add(p);
    }

    public void CreateBoolParameter(string key, bool value)
    {
        Parameter p = new BoolParameter(key, value) as Parameter;

        inkParameterList.Add(p);
    }

    public void CreateStringParameter(string key, string value)
    {
        Parameter p = new StringParameter(key, value) as Parameter;

        inkParameterList.Add(p);
    }

    public void AdjustIntParameter(string key, int adjustment)
    {
        Parameter parameterToAdjust = FindParamByKey(key);
        parameterToAdjust.Value += adjustment;
    }

    public void ChangeStringParameter(string key, string adjustment)
    {
        Parameter parameterToAdjust = FindParamByKey(key);
        parameterToAdjust.Value = adjustment;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadStorylets
{

    // Start is called before the first frame update
    public void Load()
    {

        List<Storylet> storylets = new List<Storylet>();

        TextAsset storyletData = Resources.Load<TextAsset>("Storylets");

        string[] data = storyletData.text.Split(new char[] { '\n' });

        for (int i = 1; i < data.Length - 1; i++)
        {

            string[] row = data[i].Split(new char[] { ',' });

            if (row[1] != "") {

                Storylet s = new Storylet();

                int.TryParse(row[0], out s.id);
                s.type = row[1];

                //characters go in 2
                s.character_info = row[2];


                s.location = row[3];
                s.description = row[4];

                //predicates go in 5
                s.predicates = ParsePredicates(row[2], row[5], row[4]);

                s.knotName = row[6];

                s.hasPredicates = (s.predicates.Count > 0);
                s.hasCharacterPredicates = (s.character_info != "none");
                s.usesGenericCharacter = (s.character_info.Substring(0,3) == "any");
                //Debug.Log(s.usesGenericCharacter);
               

                s.timesOffered = 0;
                s.timesVisited = 0;

                PlayerState.instance.CreateIntParameter(s.knotName, 0);

                storylets.Add(s);

            }
        }

        StoryletManager.storylets = storylets;


        //foreach (Storylet s in storylets)
        //{
        //    Debug.Log(s.description);

        //}

    }



    private List<Predicate> ParsePredicates(string characterData, string predicateData, string description)
    {
        List<Predicate> predicates = new List<Predicate>();

        //handle character predicates
        if (characterData != "none")
        {
            string[] data = characterData.Split(new char[] { ';' });

            foreach (string characterPredicate in data)
            {
                string[] info = characterPredicate.Split(new char[] { ':' });

                if (info.Length == 1)
                {
                    //it's just a character name - add their in_story property as a predicate

                    predicates.Add(new Predicate(info[0], "1", true));
                }
                else if (info.Length == 2)
                {
                    //it's of the "any" type and a boolean trait - put an underscore in front of the trait name we're looking for
                    if (info[0] == "any")
                    {
                        if (info[1].Substring(0, 1) != "!")
                        {
                            //they want it true
                            predicates.Add(new Predicate("_" + info[1], "1", true));
                        }
                        else
                        {
                            //they want it false
                            predicates.Add(new Predicate("_" + info[1], "0", true));
                        }

                    }
                    else
                    {
                        if (info[1].Substring(0, 1) != "!")
                        {
                            //they want it true
                            predicates.Add(new Predicate("sel_" + info[1], "1", true));
                        }
                        else
                        {
                            //they want it false
                            predicates.Add(new Predicate("sel_" + info[1], "0", true));
                        }
                    }


                }
                else if (info.Length == 4)
                {
                    //it's of the "any" type and asking for something that can take an integer value and an operator, like affinity


                    Predicate.PredicateOperator op;

                    switch (info[2])
                    {
                        case "equals":
                            op = Predicate.PredicateOperator.equals;
                            break;
                        case "greaterthan":
                            op = Predicate.PredicateOperator.greaterthan;
                            break;
                        case "lessthan":
                            op = Predicate.PredicateOperator.lessthan;
                            break;
                        case "greaterthanequals":
                            op = Predicate.PredicateOperator.greaterthanequals;
                            break;
                        case "lessthanequals":
                            op = Predicate.PredicateOperator.lessthanequals;
                            break;
                        default:
                            Debug.Log("incorrect formatting of operator in row " + description + " - defaulting to equals");
                            op = Predicate.PredicateOperator.equals;
                            break;
                    }

                    predicates.Add(new Predicate("_" + info[1], info[3], op, true));

                }
                else
                {
                    Debug.Log("predicate improperly formatted in row " + description);
                }

            }

        }

        //handle normal predicates
        if (predicateData != "")
        {
            string[] data = predicateData.Split(new char[] { ';' });

            foreach (string predicate in data)
            {
                string[] keyAndValue = predicate.Split(new char[] { ':' });

                if (keyAndValue.Length == 2)
                {
                    // Debug.Log("Found predicate " + keyAndValue[0] + " " + keyAndValue[1]);

                    predicates.Add(new Predicate(keyAndValue[0], keyAndValue[1], false));
                }
                else if (keyAndValue.Length == 3)
                {
                    Predicate.PredicateOperator op;

                    switch (keyAndValue[1]) {
                        case "equals":
                            op = Predicate.PredicateOperator.equals;
                            break;
                        case "greaterthan":
                            op = Predicate.PredicateOperator.greaterthan;
                            break;
                        case "lessthan":
                            op = Predicate.PredicateOperator.lessthan;
                            break;
                        case "greaterthanequals":
                            op = Predicate.PredicateOperator.greaterthanequals;
                            break;
                        case "lessthanequals":
                            op = Predicate.PredicateOperator.lessthanequals;
                            break;
                        default:
                            Debug.Log("incorrect formatting of operator in row " + description + " - defaulting to equals");
                            op = Predicate.PredicateOperator.equals;
                            break;
                    }
                    predicates.Add(new Predicate(keyAndValue[0], keyAndValue[2], op, false));

                }
                else
                {
                    Debug.Log("predicate improperly formatted in row " + description);
                }
            }
        }

        //foreach (Predicate predicate in predicates)
        //{
        //    Debug.Log(predicate.Key);
        //}

        return predicates;
    }
}

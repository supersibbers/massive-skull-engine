﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StoryletManager
{

    public static List<Storylet> storylets = new List<Storylet>();

    public static List<Storylet> AllStorylets()
    {

        return storylets;
    }

    public static List<Storylet> GetStoryletsByType(string type)
    {
        List<Storylet> filteredStorylets = new List<Storylet>();

        foreach (Storylet storylet in storylets)
        {
            if (storylet.type == type) { filteredStorylets.Add(storylet); }
        }
        return filteredStorylets;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UIController : MonoBehaviour
{

    public List<DisplayableContent> content = new List<DisplayableContent>();

    public GameObject textPrefab;
    public GameObject buttonPrefab;

    public float topMargin;

    private List<GameObject> currentActiveObjects = new List<GameObject>();

    GameManager gameManager;

    private void Start()
    {
        gameManager = GameManager.instance;
    }

    public void ContentChanged()
    {

        if (currentActiveObjects.Count > 0)
        {
            foreach (GameObject o in currentActiveObjects)
            {
                Destroy(o);
            }
        }

        currentActiveObjects = new List<GameObject>();
        

        int i = 0;

        foreach (DisplayableContent c in content)
        {
            if (c.type == DisplayableContent.ContentType.Button)
            {

                InstantiateButton(c, i);

            }
            else if (c.type == DisplayableContent.ContentType.Text)
            {

                InstantiateText(c, i);

            }
            else
            {
                Debug.Log("Unknown content type.");
            }

            i++;
        }
    }

    void InstantiateButton(DisplayableContent c, int i)
    {

        DisplayableButton _c = c as DisplayableButton;

        GameObject o = Instantiate(buttonPrefab) as GameObject;

        o.transform.SetParent(GameObject.FindGameObjectWithTag("MainContentPanel").transform);
        RectTransform rect = o.GetComponent<RectTransform>();

        rect.anchoredPosition = PositionElementByOrder(i);

        o.GetComponent<Button>().onClick.AddListener(_c.onClick);

        Text text = o.GetComponentInChildren<Text>();
        text.text = c.text;

        currentActiveObjects.Add(o);
    }

    void InstantiateText(DisplayableContent c, int i)
    {
        GameObject o = Instantiate(textPrefab) as GameObject;

        o.transform.SetParent(GameObject.FindGameObjectWithTag("MainContentPanel").transform);
        RectTransform rect = o.GetComponent<RectTransform>();

        rect.anchoredPosition = PositionElementByOrder(i);

        Text text = o.GetComponentInChildren<Text>();
        text.text = c.text;

        currentActiveObjects.Add(o);
    }

    Vector2 PositionElementByOrder(int i)
    {
        if (i == 0)
        {
            return new Vector2(0, topMargin);
        }
        else
        {

            RectTransform previousObjectTransform = currentActiveObjects[i - 1].GetComponent<RectTransform>();

            float topOfPreviousObject = previousObjectTransform.anchoredPosition.y;
            float heightOfPreviousObject;

            if (content[i-1].type == DisplayableContent.ContentType.Text)
            {
                heightOfPreviousObject = previousObjectTransform.transform.GetComponent<Text>().preferredHeight;
            }
            else
            {
                heightOfPreviousObject = previousObjectTransform.rect.height;
            }

            return new Vector2(0, topOfPreviousObject - heightOfPreviousObject);
        }
        
    }

//if (content.Count != 0)
//        {
//            if (content[0].type == DisplayableContent.ContentType.Text)
//            {
//                // create a text box with the right text in it
//                text.text = content[0].text;
//            }
//            else if (content[0].type == DisplayableContent.ContentType.Button)
//            {
//                // create a button that tells the GameManager what it needs to know
//            }
//            else
//            {
//                text.text = "oops! undefined content type!";
//            }
//        }

//        else
//        {
//            text.text = "content length is 0!";
//        }

}

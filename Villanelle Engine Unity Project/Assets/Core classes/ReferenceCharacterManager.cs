﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReferenceCharacterManager : MonoBehaviour
{
    public static ReferenceCharacterManager instance { get; private set; }

    public List<Character> characterList;

    public List<Character> charactersAvailableForNextStorylet;

    public Character currentCharacter;

    private GameManager gameManager;

    //create single instance
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {

        gameManager = GameManager.instance;

        characterList = new List<Character>();

        characterList.Add(new Character("Dagmar", "dagmar"));
        characterList.Add(new Character("Ledgermain", "ledgermain"));

        currentCharacter = characterList[0];


    }


    public void UpdateCharactersFromInkState()
    {
        //update the record of their affinities from the parameter list (easier to work with than the parameters, which aren't particularly sorted)
        foreach (Character character in characterList)
        {
            Parameter affinityParam = PlayerState.instance.FindParamByKey(character.reference + "_affinity");
            character.affinity = int.Parse(affinityParam.Value);
        }

        //update whether or not they're in the story
        foreach (Character character in characterList)
        {
            Parameter inStoryParam = PlayerState.instance.FindParamByKey(character.reference + "_in_story");
            if (inStoryParam.Value == "1")
            {
                character.inStory = true;
            }
            else
            {
                character.inStory = false;
            }
        }

    }


    public string GetCharacterRefForReferenceScene()
    {

        //currently it'll simply pick someone at random from the currently available characters, but in future it should choose more cleverly
        // the problem coming down the road here is that at the moment, this is set _only at the moment you head to the storylet_ but eventually we'll want to set it before that
        // so we can reflect that in the choice UI

        System.Random rnd = new System.Random();

        //Debug.Log(charactersAvailableForNextStorylet.Count);

        int index = rnd.Next(0, charactersAvailableForNextStorylet.Count);

        currentCharacter = charactersAvailableForNextStorylet[index];

        return currentCharacter.reference;
    }

    public Character FindCharacterByName(string name)
    {
        bool foundIt = false;

        foreach (Character character in characterList)
        {
            if (name == character.name)
            {
                foundIt = true;
            }
            else
            {
                foundIt = false;
            }

            if (foundIt)
            {
                //Debug.Log("found it");
                return character;
            }

        }
        Debug.LogWarning("You're looking for a character called " + name + " that hasn't been initialised.");
        return null;
    }

}
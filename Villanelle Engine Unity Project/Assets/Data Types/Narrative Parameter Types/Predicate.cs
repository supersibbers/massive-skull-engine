﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Predicate
{
    public string Key;
    public string Value;
    public PredicateOperator Operator;
    public bool isCharacterPredicate;

    public enum PredicateOperator {equals, greaterthan, lessthan,  greaterthanequals, lessthanequals}

    public Predicate(string key, string value, bool characterPred)
    {
        Key = key;
        Value = value;
        Operator = PredicateOperator.equals;
        isCharacterPredicate = characterPred;
    }

    public Predicate(string key, string value, PredicateOperator op, bool characterPred)
    {
        Key = key;
        Value = value;
        Operator = op;
        isCharacterPredicate = characterPred;

    }


}

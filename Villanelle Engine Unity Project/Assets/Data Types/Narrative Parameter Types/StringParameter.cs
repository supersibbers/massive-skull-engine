﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringParameter : Parameter, iParameter
{


    public StringParameter(string s, string value)
    {
        Key = s;

        Value = value;

        ValueType = Parameter.Type.String;

    }
}
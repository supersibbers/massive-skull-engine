﻿
public interface iParameter
{

    string Key { get; set; }
    string Value { get; set; }
    Parameter.Type ValueType {get; set;}
}

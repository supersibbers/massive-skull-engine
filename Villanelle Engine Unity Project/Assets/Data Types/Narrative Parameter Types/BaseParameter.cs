﻿using System;

public class Parameter
{

    public string Key { get; set; }
    public string Value { get; set; }
    public Parameter.Type ValueType { get; set; }

    public enum Type { Int, Bool, String }

}

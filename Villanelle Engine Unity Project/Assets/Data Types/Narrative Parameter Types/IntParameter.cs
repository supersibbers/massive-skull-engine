﻿using System.Collections;
using System.Collections.Generic;

public class IntParameter : Parameter, iParameter
{


    public IntParameter(string s, int value)
    {
        Key = s;

        Value = value.ToString();

        ValueType = Parameter.Type.Int;

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoolParameter : Parameter, iParameter

{


    public BoolParameter(string s, bool value)
    {

        Key = s;

        if (value)
        {
            Value = "1";
        }
        else
        {
            Value = "0";
        }


        ValueType = Parameter.Type.Bool;

    }

}


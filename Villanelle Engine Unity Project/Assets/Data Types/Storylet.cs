﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Storylet
{
    //written from spreadsheet
    public int id;
    public string type;
    public string character_info;
    public string location;
    public string description;
    public List<Predicate> predicates;
    public string knotName;

    //written to at runtime
    public int timesVisited;
    public int timesOffered;
    public bool hasPredicates;
    public bool hasCharacterPredicates;
    public bool usesGenericCharacter;
    public List<Character> currentAvailableCharacters;



}

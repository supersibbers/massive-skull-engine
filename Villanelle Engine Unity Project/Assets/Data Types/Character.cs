﻿using System.Collections;
using System.Collections.Generic;

public class Character
{
    public string name;
    public string reference;
    public int affinity;
    public bool inStory;

    public Character(string n, string r)
    {
        name = n;
        reference = r;
        affinity = 0;
        inStory = false;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayableContent
{
    // an object that gets created by the game manager - it contains all the stuff we need to display (text and buttons, in some kind of order)
    // maybe it's an interface that can be either words or buttons?

    public enum ContentType {Button, Text}

    public string text;
    public ContentType type;

}

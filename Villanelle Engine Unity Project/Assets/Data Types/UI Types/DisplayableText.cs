﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayableText : DisplayableContent
{
    public DisplayableText()
    {
        type = ContentType.Text;
        text = "";

    }

    public DisplayableText(string c)
    {
        type = ContentType.Text;
        text = c;

    }
}

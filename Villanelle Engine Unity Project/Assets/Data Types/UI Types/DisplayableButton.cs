﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DisplayableButton : DisplayableContent
{

    public UnityAction onClick;

    public DisplayableButton()
    {
        type = ContentType.Button;
        text = "";
        
    }

    public DisplayableButton(string c, UnityAction a)
    {
        type = ContentType.Button;
        text = c;
        onClick = a;
    }
 
}
